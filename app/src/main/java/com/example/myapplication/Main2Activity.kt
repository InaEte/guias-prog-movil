package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main2.*
import android.graphics.Color

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        serviceButton.setOnClickListener{

            showSelectionText.setText("Boton seleccionado: Servicios")

        }
        portfolioButton.setOnClickListener{
            showSelectionText.setText("Boton seleccionado: Portafolio")
        }
        aboutButton.setOnClickListener{
            showSelectionText.setText("Boton seleccionado: Acerca de")
        }
        contactButton.setOnClickListener{
            showSelectionText.setText("Boton seleccionado: Contacto")
        }
        socialMediaButton.setOnClickListener{
            showSelectionText.setText("Boton seleccionado: Redes Sociales")
        }
    }
}
